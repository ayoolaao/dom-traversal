const grandParent = document.querySelector('.grandparent');
const parents = Array.from(document.getElementsByClassName('parent'));
const childOne = parents[0].firstElementChild
const lastChild = parents[parents.length -1].lastElementChild
const parentAgain = lastChild.closest('.parent')

// functions
const changeColor = (element, hexValue) => {
  element.style.background = hexValue
}

changeColor(grandParent, '#333')
parents.forEach(parent => changeColor(parent, '#ae01d7'))
changeColor(childOne, '#333')
changeColor(lastChild, '#333')
changeColor(parentAgain, '#333')
